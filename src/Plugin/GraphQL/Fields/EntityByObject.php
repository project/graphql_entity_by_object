<?php

namespace Drupal\graphql_entity_by_object\Plugin\GraphQL\Fields;

use Drupal\Core\TypedData\TranslatableInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * This just returns an entity, that was passed in by this entity serialized.
 *
 * This field can be used, for example, when you want to display a new paragraph
 * in the backend. Otherwise its not possible to display the unsaved paragraph
 * since its not possible to query an unsaved entity. (at least we found no
 * possibility).
 * Since its not possible to pass an object as argument, its necessary that the
 * entity will be passed serialized.
 *
 *
 * @GraphQLField(
 *   id = "entity_by_object",
 *   secure = true,
 *   name = "entityByObject",
 *   type = "Entity",
 *   arguments = {
 *     "serializedObject" = "String"
 *   },
 *   contextual_arguments = {"language"}
 * )
 */
class EntityByObject extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if (!empty($args['serializedObject'])) {
      $entity = unserialize($args['serializedObject']);

      // Translate entity into context language if translation exists.
      if (isset($args['language']) && $args['language'] != $entity->language()->getId() && $entity instanceof TranslatableInterface && $entity->isTranslatable()) {
        if ($entity->hasTranslation($args['language'])) {
          $entity = $entity->getTranslation($args['language']);
        }
      }
      yield $entity;
    }
  }

}
